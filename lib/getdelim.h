/* #included only if getdelim() is lacking. */

#include <sys/types.h>          /* size_t */
#include <stdio.h>              /* FILE */

long getdelim(char **LINEPTR, size_t * N, int delim, FILE * STREAM);
