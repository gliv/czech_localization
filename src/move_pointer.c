/*********************
 * Extension to GDK. *
 *********************/

#include <gdk/gdkx.h>

#include "move_pointer.h"

#define EVENT_MASK Button1MotionMask | Button4MotionMask | ButtonMotionMask  | \
                   Button2MotionMask | Button5MotionMask | PointerMotionMask | \
                   Button3MotionMask | PointerMotionHintMask

/*
 * Move the pointer to the (x, y) coordinates of the root window
 * without respect to the current pointer position.
 */
void move_pointer(gint dest_x, gint dest_y)
{
    Display *dpy;
    XEvent xev;

    dpy = GDK_DISPLAY();

    while (XCheckMaskEvent(dpy, EVENT_MASK, &xev) == True);

    XWarpPointer(dpy, None, GDK_ROOT_WINDOW(), 0, 0, 0, 0, dest_x, dest_y);
}
