#ifndef DITHERING_H
#define DITHERING_H

#include "gliv.h"

void dither_pixbuf(GdkPixbuf * dest, GdkPixbuf * src);

#endif
