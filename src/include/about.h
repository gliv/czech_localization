#ifndef ABOUT_H
#define ABOUT_H

#include "messages.h"

#define ABOUT_GLIV       N_("GLiv version")

#define ABOUT_HELP       N_("Use 'gliv --help' to show command line options.\nA quick help is accessible by typing 'h' or using the menu.")

#define ABOUT_URL        N_("See http://guichaz.free.fr/gliv for updates.")

#endif
