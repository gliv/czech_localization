#ifndef PATHSET_H
#define PATHSET_H

#include "gliv.h"

struct pathset {
    GTree *tree;
};

struct pathset *pathset_new(void);
gboolean pathset_add(struct pathset *set, const gchar * path);
void pathset_free(struct pathset *set);

#endif
