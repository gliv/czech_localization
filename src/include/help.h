#ifndef HELP_H
#define HELP_H

#include "gliv.h"

gboolean toggle_help(void);
GdkPixbuf *get_gliv_logo(void);
gboolean show_about_box(void);

#endif
