#ifndef CALLBACKS_H
#define CALLBACKS_H

#include "gliv.h"

void install_callbacks(gpointer object);
void process_events(void);

#endif
