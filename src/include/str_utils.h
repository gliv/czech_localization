#ifndef STR_UTILS_H
#define STR_UTILS_H

#include "gliv.h"

#if SIZEOF_UNSIGNED_LONG == 4
#define LONG_MASK(l32, l64) (l32)
#elif SIZEOF_UNSIGNED_LONG == 8
#define LONG_MASK(l32, l64) (l64)
#else
#error "sizeof(unsigned long) is neither 4 nor 8"
#endif

/*
 * The magic to find a '\0' in a long int is taken from the glibc.
 * See sysdeps/generic/strlen.c in the glibc sources to have the
 * explanation.
 */
#define MAGIC_BITS LONG_MASK(0x7efefeffL, 0x7efefefefefefeffL)


#define HAS_ZERO(lint) ((((lint) + MAGIC_BITS) ^ ~(lint)) & ~MAGIC_BITS)

#define NOT_ALIGNED(ptr) (((gulong) (ptr)) & (sizeof(gulong) - 1))

gint common_prefix_length(const gchar * str1, const gchar * str2);
const gchar *filename_to_utf8(const gchar * str);
gchar *clean_filename(const gchar * filename);

#endif
