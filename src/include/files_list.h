#ifndef FILES_LIST_H
#define FILES_LIST_H

#include "gliv.h"
#include "gliv-image.h"
#include "timestamp.h"

gint get_list_length(void);
GList *get_list_head(void);
GList *get_list_end(void);
void remove_from_list(GList * node);
gboolean reorder_files(gboolean shuffle);
gchar **get_sorted_files_array(void);
gint init_from_null_filenames(gboolean sort, gboolean shuffle,
                              gboolean add_all);
gint init_list(gchar ** names, gint nb, gboolean sort, gboolean shuffle,
               gboolean add_all);
gint insert_after_current(gchar ** names, gint nb, gboolean just_file,
                          gboolean add_all);
gboolean confirm_remove_current(void);
timestamp_t get_list_timestamp(void);
const gchar *get_nth_filename(gint n);
gint get_image_number(GlivImage * im);
GList *find_node_by_name(const gchar * name);
void add_obsolete_node(GList *node);
gboolean remove_obsolete_nodes(void);

#endif
