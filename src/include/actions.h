#ifndef ACTIONS_H
#define ACTIONS_H

#include <stdio.h>
#include "gliv.h"

void init_actions(GtkAccelGroup * the_accel_group,
                  GtkMenuItem * current_image_menu_item,
                  GtkMenuItem * every_image_menu_item);
gboolean edit_actions(void);
void add_action(const gchar * name, const gchar * command);
void write_actions(FILE * file);

#endif
