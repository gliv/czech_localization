#ifndef FORMATS_H
#define FORMATS_H

#include "gliv.h"

/* Loader according to filename extension. */
typedef enum {
    LOADER_NONE,                /* Unrecognized extension. */
    LOADER_PIXBUF,              /* GdkPixbuf. */
    LOADER_DOT_GLIV,            /* GLiv collection. */
    LOADER_DECOMP,              /* Decompression first. */
    LOADER_DECOMP_PIXBUF,       /* Compressed image. */
    LOADER_DECOMP_DOT_GLIV      /* Compressed collection. */
} loader_t;

struct format {
    const gchar *name;
    const loader_t loader;
};

const struct format *ext_to_loader(const gchar * str, guint len);

#endif
