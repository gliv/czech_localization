#ifndef SCROLLBARS_H
#define SCROLLBARS_H

#include "gliv.h"

gboolean toggle_scrollbars(void);
void update_scrollbars(void);
GtkWidget *get_new_scrollbar(gboolean horizontal);
void hide_scrollbars(void);

#endif
