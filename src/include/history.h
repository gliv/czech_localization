#ifndef HISTORY_H
#define HISTORY_H

void clean_history(void);
void append_history(void);
gboolean undo(void);
gboolean redo(void);
gboolean clear_history(void);

#endif
