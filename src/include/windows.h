#ifndef WINDOWS_H
#define WINDOWS_H

#include "gliv.h"
#include "gliv-image.h"

void show_message(GtkWidget * widget, const gchar * name);
gint run_modal_dialog(GtkDialog * dialog);
void set_loading_entry(const gchar * filename);
gboolean toggle_widgets(GtkWidget ** widgets, gint nb_widgets, gboolean * flag);
gboolean toggle_menu_bar(void);
gboolean toggle_status_bar(void);
void update_status_bar(void);
void create_windows(void);
void goto_window(GlivImage * im, gboolean first_time);
void toggle_fullscreen(gboolean enable);
void update_window_title(void);
GtkWindow *get_current_window(void);
GtkWindow *new_window(const gchar * title);
gboolean toggle_floating_windows(void);

#endif
