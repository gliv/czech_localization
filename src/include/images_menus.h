#ifndef IMAGES_MENUS_H
#define IMAGES_MENUS_H

#include "gliv.h"

GNode *get_tree(void);
void set_rebuilding_entry(GtkMenuItem * item);
void set_progress(const gchar * menu, gint * percent, gint number);
gboolean rebuild_directories(GtkMenuItem * root);
gboolean rebuild_images(GtkMenuItem * root);
gboolean rebuild_images_menus(void);
void stop_rebuilding(void);
void set_stop_rebuilding_menu(GtkMenuItem * item);
void obsolete_menus(void);
void cond_rebuild_menus(void);

#endif
