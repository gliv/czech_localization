#ifndef FOREACH_FILE_H
#define FOREACH_FILE_H

#include "gliv.h"

typedef gint(*foreach_file_func) (const gchar * path);

gint foreach_file(const gchar * path, foreach_file_func func);

#endif
