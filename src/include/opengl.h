#include <GL/gl.h>
#include <gtk/gtkgl.h>
#include "gliv.h"

#if HAVE_ISO_C_VARARGS

#include "options.h"
#include "gl_widget.h"

#define NO_OPENGL 0

#if NO_OPENGL
#define gdk_gl_drawable_wait_gdk(...)
#define gtk_widget_get_gl_context(...) NULL
#define gtk_widget_get_gl_drawable(...) NULL
#define gdk_gl_drawable_make_current(...) TRUE
#define gdk_gl_query_extension(...) TRUE
#define gdk_gl_config_new_by_mode(...) (void*) 1
#define gtk_widget_set_gl_capability(...) TRUE
#define gtk_gl_init(...)
#define gtk_widget_get_gl_drawable(...) NULL
#define gdk_gl_drawable_wait_gdk(...)
#define gdk_gl_drawable_swap_buffers(...)
#define gdk_gl_drawable_wait_gl(...)

#define OPENGL_VOID_CALL_VOID(func)
#define OPENGL_VOID_CALL(func, ...)
#define OPENGL_CALL(return_t, func, ...) 1
#define glBegin(mode)
#define glEnd()
#else
#define report_errors(f, inc)                                   \
    if (options->opengl_errors)                                 \
        report_opengl_errors(f, __FILE__, __LINE__, inc)

#define OPENGL_VOID_CALL_VOID(func)             \
    do {                                        \
        func();                                 \
        report_errors(#func, 0);                \
    } while (0)

#define OPENGL_VOID_CALL(func, ...)             \
    do {                                        \
        func(__VA_ARGS__);                      \
        report_errors(#func, 0);                \
    } while (0)

#define OPENGL_CALL(return_t, func, ...)        \
    ({                                          \
        return_t res = func(__VA_ARGS__);       \
        report_errors(#func, 0);                \
        res;                                    \
    })

/* Special cases ... */

#define glBegin(mode)                           \
    do {                                        \
        glBegin(mode);                          \
        report_errors("glBegin", 1);            \
    } while (0)

#define glEnd()                                 \
    do {                                        \
        glEnd();                                \
        report_errors("glEnd", -1);             \
    } while (0)

#endif
#include "opengl_wrapper.h"
#else
#warning "No variadic macros"
#endif
