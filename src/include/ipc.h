#ifndef IPC_H
#define IPC_H

#include "gliv.h"

gboolean start_server(void);
gboolean connect_server(gboolean clear_list);

#endif
