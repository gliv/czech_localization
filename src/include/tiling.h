#ifndef TILING_H
#define TILING_H

struct tiles {
    gint *array;
    gint waste;
    gint nb_tiles;
};

struct tiles_iterator {
    struct tiles *tiles;
    gboolean first_pass;
    gint current_array_pos;
    gint remaining_in_pos;
};

void destroy_tiles(struct tiles *tiles);
struct tiles *make_tiles(gint dim);
struct tiles_iterator *tiles_iterator_new(struct tiles *tiles);
gint tiles_iterator_next(struct tiles_iterator *iter);

#endif
