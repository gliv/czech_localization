#ifndef MENUS_H
#define MENUS_H

#include "gliv.h"

void set_menu_label(GtkMenuItem * item, const gchar * text, gboolean mnemonic);
gboolean menu_set_top_left(void);
GtkAccelGroup *create_menus(void);

#endif
