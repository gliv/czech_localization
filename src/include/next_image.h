#ifndef NEXT_IMAGE_H
#define NEXT_IMAGE_H

#include "gliv.h"
#include "gliv-image.h"

void after_reorder(void);
const gchar *get_image_notice(void);
void load_direction(gint dir);
void load_random_image(void);
void load_1st_image(void);
void load_last_image(void);
void load_first_image(void);
void reload_current_image(void);
void reload_images(void);
gboolean menu_load(const gchar * filename);
void unload(GList * node);
void unload_images(void);
gpointer stat_loaded_files(gboolean quiet);
void reload_changed_files(gpointer * stat_data);
void set_slide_show_menu(GtkMenuItem * item);
void start_slide_show(void);
gboolean slide_show_started(void);
gboolean toggle_slide_show(void);
void new_images(gint nb_inserted);

#endif
