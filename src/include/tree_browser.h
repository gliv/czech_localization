#ifndef TREE_BROWSER_H
#define TREE_BROWSER_H

#include "gliv.h"

void load_later(void);
void show_tree_browser(void);
void highlight_current_image(void);
void cond_rebuild_tree_browser(void);

#endif
