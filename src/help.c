/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * See the COPYING file for license information.
 *
 * Guillaume Chazarain <guichaz@yahoo.fr>
 */

/****************************
 * The help and about boxes *
 ****************************/

#include <string.h>             /* strlen() */
#include <gdk-pixbuf/gdk-pixdata.h>

#include "gliv.h"
#include "help.h"
#include "about.h"
#include "options.h"
#include "params.h"
#include "help_text.h"
#include "windows.h"

extern rt_struct *rt;

static GtkWindow *help_win;

static gchar *get_help_text(void)
{
    gchar **help_lines, **ptr;
    gchar *res, *res_ptr;
    gint len = 0;

    help_lines = get_help_lines();

    /* Compute the total text length. */
    for (ptr = help_lines; *ptr != NULL; ptr++)
        len += strlen(*ptr);

    res_ptr = res = g_new(gchar, len + 1);

    /* Concatenate the lines. */
    for (ptr = help_lines; *ptr != NULL; ptr++)
        res_ptr = g_stpcpy(res_ptr, *ptr);

    g_free(help_lines);

    return res;
}

/* Get a textual widget with the help text in it. */
static GtkWidget *get_text_widget(const gchar * text)
{
    GtkTextView *widget;

    widget = GTK_TEXT_VIEW(gtk_text_view_new());

    gtk_text_view_set_editable(widget, FALSE);
    gtk_text_view_set_cursor_visible(widget, FALSE);
    gtk_text_view_set_wrap_mode(widget, GTK_WRAP_WORD);
    gtk_text_buffer_set_text(gtk_text_view_get_buffer(widget), text, -1);

    return GTK_WIDGET(widget);
}

static void show_help(void)
{
    PangoFontDescription *font;
    gchar *help_text;
    GtkWidget *widget;
    GtkScrolledWindow *win;

    help_text = get_help_text();

    widget = get_text_widget(help_text);
    g_free(help_text);

    /* We use a fixed font to keep the alignment as in the README file. */
    font = pango_font_description_from_string(FONT);
    gtk_widget_modify_font(GTK_WIDGET(widget), font);

    /* The window containing the text. */
    win = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
    gtk_scrolled_window_set_policy(win,
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_container_add(GTK_CONTAINER(win), widget);

    /* The help window. */
    help_win = new_window(_("GLiv help"));
    gtk_window_set_default_size(help_win, 500, 500);

    gtk_container_add(GTK_CONTAINER(help_win), GTK_WIDGET(win));

    g_signal_connect(help_win, "delete-event", G_CALLBACK(toggle_help), NULL);

    show_message(GTK_WIDGET(help_win), _("GLiv help"));
}

gboolean toggle_help(void)
{
    rt->help ^= TRUE;

    if (rt->help)
        show_help();
    else
        gtk_widget_destroy(GTK_WIDGET(help_win));

    return TRUE;
}

#include "include/gliv_logo.h"

GdkPixbuf *get_gliv_logo(void)
{
    static GdkPixbuf *logo;

    if (logo == NULL)
        /* First time */
        logo = gdk_pixbuf_from_pixdata(&gliv_logo, FALSE, NULL);

    return logo;
}

gboolean show_about_box(void)
{
    GtkDialog *dialog = NULL;
    GtkLabel *about;
    GtkHBox *box;
    gchar *about_text;
    GdkPixbuf *logo;
    GtkImage *image;

    /* Logo */
    logo = get_gliv_logo();
    if (logo == NULL)
        return FALSE;

    image = GTK_IMAGE(gtk_image_new_from_pixbuf(logo));

    /* Text */
    about_text = g_strconcat(_(ABOUT_GLIV), " ", VERSION, "\n",
                             "Guillaume Chazarain <guichaz@yahoo.fr>\n",
                             _(ABOUT_HELP), "\n",
                             "\n", _(ABOUT_URL), "\n", NULL);

    about = GTK_LABEL(gtk_label_new(about_text));
    g_free(about_text);

    /* Dialog */
    dialog = GTK_DIALOG(gtk_dialog_new_with_buttons(_("GLiv about box"),
                                                    get_current_window(),
                                                    GTK_DIALOG_MODAL,
                                                    GTK_STOCK_CLOSE,
                                                    GTK_RESPONSE_ACCEPT, NULL));

    box = GTK_HBOX(gtk_hbox_new(FALSE, 10));
    gtk_box_pack_start_defaults(GTK_BOX(box), GTK_WIDGET(image));
    gtk_box_pack_start_defaults(GTK_BOX(box), GTK_WIDGET(about));
    gtk_widget_show_all(GTK_WIDGET(box));

    gtk_container_add(GTK_CONTAINER(dialog->vbox), GTK_WIDGET(box));

    run_modal_dialog(dialog);

    gtk_widget_destroy(GTK_WIDGET(dialog));
    return FALSE;
}
