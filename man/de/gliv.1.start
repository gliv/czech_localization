.\" 
.TH "gliv" "1" "1.9" "Guillaume Chazarain" "Image viewer"
.SH "NAME"
gliv \- Ein OpenGL Image Viewer

.SH "SYNOPSIS"
\fBgliv\fR [OPTIONS]... [FILES]...

.SH "BESCHREIBUNG"
\fIgliv\fR verwendet gdk\-pixbuf um Images zu laden, und OpenGL zur Darstellung.

Gliv ermöglicht es, Images zu bewegen, rotieren, zoomen, und durchführen von Slide Shows.

.SH "OPTIONS"
Die Optionen sind zuerst auf ihre Defaultwerte eingestellt, "off" für Flags, werden dann entweder von ~/.glivrc oder /etc/glivrc oder einem Konfigurationsfile das über die Kommandozeile spezifiert wird, gelesen, oder falls vorhanden, von der Kommandozeile gelesen.

.br 

Weglassen des Arguments für eine Option, die ein "on|off" Wertebereich hat, ist gleichbedeutend mit "on", also \-\-foo ist gleichbedeutend mit \-\-foo=on (wenn foo die Werte on|off annehmen kann).

.TP 

\fB\-h, \-\-help\fR

Anzeigen des Hilfetextes und Ende.

.TP 

\fB\-V, \-\-version\fR

Anzeige der Version und Ende.

.TP 

\fB\-a, \-\-add\-all[=on|off]\fR

Hinzufügen aller Files im momentanen Directory. Mit dieser Option werden bei Öffnen eines Files (entweder von der Kommandozeile oder mit dem "open" Dialog) alle anderen Files im Directory hinzugefügt.

.TP 

\fB\-R, \-\-recursive[=on|off]\fR

Rekursiv alle Unterverzeichnisse miteinbeziehen. Wenn \fIgliv\fR alle Images eines Verzeichnisses lädt, werden rekursiv auch alle Images aller Unterverzeichnisse geladen.

.TP 

\fB\-S, \-\-sort[=on|off]\fR

Images sortiert anzeigen. Die Images werden vor der Slideshow sortiert.

.TP 

\fB\-s, \-\-shuffle[=on|off]\fR

Images in zufälliger Reihenfolge anzeigen. Die Slideshow wird in zufälliger Reihenfolge durchgeführt.

.TP 

\fB\-F, \-\-force\-load[=on|off]\fR

Versuchen jedes File zu laden. Beim Laden eines Files wird anhand der Filenamenserweiterung (Extension, z.B . .jpg) entschieden, was für ein Lader verwendet wird. Bei unbekannter Extension wird ein File ignoriert, durch Angabe dieser Option versucht \fIgliv\fR jedes File zu laden.

.TP 

\fB\-C, \-\-client[=on|off]\fR

Verbindung zu einem bereits laufenden \fIgliv\fR herstellen. Durch Verwendung dieser Option öffnet die \fIgliv\fR Instanz im Serverfenster zusätzlich die Files die als Argumente auf der Kommandozeile übergeben werden. It can also be used with the \-0 option. Der \fIgliv\fR Server ist entweder die zuletzt gestartete \fIgliv\fR Instanz, oder diejenige die im "Options" Menü ausgewählt wurde.

.TP 

\fB\-c, \-\-client-clear[=on|off]\fR

Connect to a running \fIgliv\fR, replacing the list. This is like the \-\-client option except that the specified list on the client will replace the list on the server instead of being appended to.

.TP 

\fB\-e, \-\-build\-menus[=on|off]\fR

Unterdrücke das "Images" Menü beim Start. Dies beschleunigt den Start von \fIgliv\fR, vor allem mit vielen Files als Argument auf der Kommandozeile.

.TP 

\fB\-g, \-\-glivrc[=FILE]\fR

Verwende das angegebene File als Konfigurationsfile (oder keines). Dies unterdrückt die Abarbeitung vorhandener Konfigurationsfiles (~/.glivrc und /etc/glivrc).

.TP 

\fB\-w, \-\-slide\-show[=on|off]\fR

Slideshow sofort starten. Durch Angabe dieser Option ist ein manueller Start der Slideshow im Menü nicht erforderlich.

.TP 

\fB\-0, \-\-null[=on|off]\fR

Lese null\-terminierte Filenamen. Die kann zusammen mit "find \-print0" oder mit "tr '\en' '\e0'" verwendet werden, wenn eine sehr lange Liste von Filenamen an \fIgliv\fR übergeben werden soll. Im Gegensatz zu xargs(1) ist so eine unendliche Anzahl von Filenamen möglich.

.TP 

\fB\-o, \-\-collection[=FILE]\fR

Output a collection. With this option, \fIgliv\fR creates a collection from the loaded files and outputs it to stdout or in the specified file.


.TP 

\fB\-G, \-\-geometry=GEOMETRY\fR

Initial window geometry. This option can be used to specify the position and dimension of the \fIgliv\fR window. It expects a geometry argument in the XParseGeometry(3X11) format like: 640x480+20\-30 for example.
.SH "COLLECTIONS"
Seit Version 1.8 unterstützt \fIgliv\fR ein Fileformat namens "GLiv collection". Es enthält eine Liste aller Images und der entsprechenden Thumbnails. Auf diesem Weg wird der Aufbau des "Images" Menüs beschleunigt, da die Thumbnails nicht mehr neu generiert werden müssen.

.br 

Für diese Collections wird \fIgliv\fR auch transparente Decompression unterstützt, so daß die Collections mittels bzip2, gzip or compress(1) komprimiert werden können (vorausgesetzt der entsprechende Decompressor ist installiert).

.SH "CONTROLS"

