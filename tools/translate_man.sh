#!/bin/sh

L=$1

if [ "$L" = . ]; then
    awk -f ../../tools/help2man.awk ../../README | sed 's/gliv/\\fIgliv\\fR/g'
    exit $?
fi

TEMP_DIR=/tmp/gliv-$$.$RANDOM
DIR=$L/LC_MESSAGES/

mkdir -p "${TEMP_DIR}/${DIR}" &&
cp ../../po/${L}.gmo "${TEMP_DIR}/${DIR}/gliv.mo"

export TEXTDOMAINDIR="$TEMP_DIR" LANGUAGE="$L"

awk -f ../../tools/help2man.awk ../../README |
while read LINE; do
    if [ "$LINE" = "" ] || [ "$LINE" = ".br" ]; then
        echo "$LINE"
    else
        gettext gliv "$LINE" | sed 's/gliv/\\fIgliv\\fR/g'
        echo
    fi
done

rm -fr "$TEMP_DIR"
