#include <gtk/gtk.h>


void
on_add_button_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_property_button_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_delete_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);
