$0 == "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" {
    if (started)
        # The end.
        exit

    # The beginning.
    started = 1
    getline
}

/.*: .*/ { # A keyboard accelerator line.
    if (started) {
        print
        print ".br"
    }
}

$0 == "" {
    if (started) {
        # End of keyboard accelerators.
        misc = 1
        RS = "\n\n"
        getline
        print ""
    }
}

{
    if (misc) {
        gsub(/(\n|  )/, " ")
        print
        print ".br\n"
    }
}
