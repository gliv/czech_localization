#!/bin/bash

TMPFILE=$$.$RANDOM.c
OBJECT=$(basename "$TMPFILE" .c).o

for FILE in "$@"; do
    echo "$FILE: "
    for INCLUDE in $(gawk '
BEGIN {
    FS = "\""
}

/^#include \"/ {
    print $2
}' < "$FILE"); do
	rm "$TMPFILE" "$OBJECT" &> /dev/null
	grep -vE "^#include \"$INCLUDE\"$" < "$FILE" > "$TMPFILE"
	COUNT=$( (make "$OBJECT" 2>&1 || echo "a\nb\nc") | wc -l)
	[ "$COUNT" -eq 2 ] && echo "$INCLUDE"
    done
done

rm "$TMPFILE" "$OBJECT" &> /dev/null
